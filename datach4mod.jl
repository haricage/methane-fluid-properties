module m
    # critical terms and values for CO2
    tc = 190.564;              # critical temperature in K
    pc = 4.599;                # critical pressure in MPa
    rhoceos = 159.97;           # critical density in kg/m3
    rhoc = 162.66;               # critical density in kg/m3
    rmol = 8.314510;            # molar gas constant in j/mol/k
    molwt = 16.043;            # molecular weight in g/mol
    r = rmol/molwt;             # specific gas constant in j/g/K
    # n constants
    n1 = 0.89269676; n2 = -2.5438282; n3 = 0.64980978; n4 = 0.020793471;
    n5 = 0.070189104; n6 = 0.00023700378; n7 = 0.16653334; n8 = -0.043855669;
    n9 = -0.15726780; n10 = -0.035311675; n11 = -0.029570024; n12 = 0.014019842;
    # d constants
    d1 = 1.00; d2 = 1.00; d3 = 1.00; d4 = 2.00; d5 = 3.00; d6 = 7.00;
    d7 = 2.00; d8 = 5.00; d9 = 1.00; d10 = 4.00; d11 = 3.00; d12 = 4.00;
    # t constants
    t1 = 0.25; t2 = 1.125; t3 = 1.50; t4 = 1.375; t5 = 0.25; t6 = 0.875; t7 = 0.625;
    t8 = 1.750; t9 = 3.625; t10 = 3.625; t11 = 14.5; t12 = 12.00;
    # p constants
    p7 = 1.0; p8 = 1.0; p9 = 2.0; p10 = 2.0; p11 = 3.0; p12 = 3.0;
    # a constants
    a1 = 9.91243972; a2 = -6.33270087; a3 = 3.0016; a4 = 0.008449;
    a5 = 4.6942; a6 = 3.4865; a7 = 1.6572; a8 = 1.4115;
    # theta constants
    th4 = 3.40043240; th5 = 10.26951575; th6 = 20.43932747; th7 = 29.93744884; th8 = 79.13351945;
end