# CH4 Fluid Properties Prediction

This is a readme file for swpch4.jl. Code written by Hariharan Ramachandran.<br>

The documentation is available in the page -- https://haricage.gitlab.io/methane-fluid-properties/

**Usage**<br>
This [Julia](https://julialang.org/) code calculates the fluid properties of CH4 based on the [Span-Wagner Technical Equation Of State](https://link.springer.com/article/10.1023/A:1022390430888) (Multiparameter EOS Formulation).
Run swpch24in the runtime environment. In the Julia Repl window, type --> swdench4(Pressure in MPa, Temperature in K). The density output is in kg/m3.The first term in the output represents the gas phase and the second term represents the liquid phase.<br>

**Density**<br>
swdench4(Pressure in MPa, Temperature in K). The density output is in kg/m3.<br>
Example --> swdench4(16,287)<br>
Output --> (137.1428840331516, 0.0)<br>

**Enthalpy**<br>
swenthch4(Pressure in MPa, Temperature in K). The enthalpy output is in kj/kg.<br>
Example --> swenthch4(16,287)<br>
Output --> (-195.01194010655604, 0.0)<br>

**Internal energy**<br>
swintech4(Pressure in MPa, Temperature in K). The internal energy output is in kj/kg.<br>
Example --> swenthch4(16,287)<br>
Output --> (-311.67858374983933, 0.0)<br>


<h3>Acknowledgement</h3>

This work was supported by the [SEAMSTRESS project](https://site.uit.no/seamstress/). The SEAMSTRESS project is supported by the Tromsø Research Foundation (TFS) and the Research Council of Norway (RCN-Frinatek) through two starting grants awarded to Andreia Plaza-Faverola. In addition, the Faculty of Science and Technology, the Department of Geosciences at UiT, and the Center for Arctic gas hydrates, environment and climate (CAGE) provide significant support to the project.
