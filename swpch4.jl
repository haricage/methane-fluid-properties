#=
MIT License

Copyright (c) 2022 Hariharan Ramachandran

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
=#

include("datach4mod.jl")
#--------------------------------------------------------------------------
# All the subsequent functions are calculated based on equations from span
# wagner book/ papers (reduced form)
    # Function to calculate alpha residual
    function alfr(tau,del)
        alphar1 = m.n1*del^m.d1*tau^m.t1+m.n2*del^m.d2*tau^m.t2+m.n3*del^m.d3*tau^m.t3+m.n4*del^m.d4*tau^m.t4+m.n5*del^m.d5*tau^m.t5+m.n6*del^m.d6*tau^m.t6;
        alphar2 = m.n7*del^m.d7*tau^m.t7*exp(-del^m.p7)+m.n8*del^m.d8*tau^m.t8*exp(-del^m.p8);
        alphar3 = m.n9*del^m.d9*tau^m.t9*exp(-del^m.p9)+m.n10*del^m.d10*tau^m.t10*exp(-del^m.p10);
        alphar4 = m.n11*del^m.d11*tau^m.t11*exp(-del^m.p11)+m.n12*del^m.d12*tau^m.t12*exp(-del^m.p12);
        alphar = alphar1+alphar2+alphar3+alphar4;
        return alphar
    end
#--------------------------------------------------------------------------
    # Function to calculate differential of alpha residual w.r.t delta
    function dalfrdel(tau,del)
        alphar1 = m.n1*m.d1*del^(m.d1-1.0)*tau^m.t1+m.n2*m.d2*del^(m.d2-1.0)*tau^m.t2+m.n3*m.d3*del^(m.d3-1.0)*tau^m.t3;
        alphar2 = m.n4*m.d4*del^(m.d4-1.0)*tau^m.t4+m.n5*m.d5*del^(m.d5-1.0)*tau^m.t5+m.n6*m.d6*del^(m.d6-1.0)*tau^m.t6;
        alphar3 = m.n7*del^(m.d7-1.0)*(m.d7-m.p7*del^m.p7)*tau^m.t7*exp(-del^m.p7);
        alphar4 = m.n8*del^(m.d8-1.0)*(m.d8-m.p8*del^m.p8)*tau^m.t8*exp(-del^m.p8)+m.n9*del^(m.d9-1.0)*(m.d9-m.p9*del^m.p9)*tau^m.t9*exp(-del^m.p9);
        alphar5 = m.n10*del^(m.d10-1.0)*(m.d10-m.p10*del^m.p10)*tau^m.t10*exp(-del^m.p10)+m.n11*del^(m.d11-1.0)*(m.d11-m.p11*del^m.p11)*tau^m.t11*exp(-del^m.p11);
        alphar6 = m.n12*del^(m.d12-1.0)*(m.d12-m.p12*del^m.p12)*tau^m.t12*exp(-del^m.p12);
        alphar = alphar1+alphar2+alphar3+alphar4+alphar5+alphar6;
        return alphar
    end
#--------------------------------------------------------------------------
    # Function to calculate  double differential of alpha residual w.r.t delta
    function d2alfrdel(tau,del)
        alphar1 = m.n1*m.d1*(m.d1-1.0)*del^(m.d1-2.0)*tau^m.t1+m.n2*m.d2*(m.d2-1.0)*del^(m.d2-2.0)*tau^m.t2+m.n3*m.d3*(m.d3-1.0)*del^(m.d3-2.0)*tau^m.t3;
        alphar2 = m.n4*m.d4*(m.d4-1.0)*del^(m.d4-2.0)*tau^m.t4+m.n5*m.d5*(m.d5-1.0)*del^(m.d5-2.0)*tau^m.t5+m.n6*m.d6*(m.d6-1.0)*del^(m.d6-2.0)*tau^m.t6;
        alphar6 = 0.0;
        alphar7 = m.n7*del^(m.d7-2.0)*((m.d7-m.p7*del^m.p7)*(m.d7-1.0-m.p7*del^m.p7)-m.p7^2.0*del^m.p7)*tau^m.t7*exp(-del^m.p7);
        alphar8 = m.n8*del^(m.d8-2.0)*((m.d8-m.p8*del^m.p8)*(m.d8-1.0-m.p8*del^m.p8)-m.p8^2.0*del^m.p8)*tau^m.t8*exp(-del^m.p8);
        alphar9 = m.n9*del^(m.d9-2.0)*((m.d9-m.p9*del^m.p9)*(m.d9-1.0-m.p9*del^m.p9)-m.p9^2.0*del^m.p9)*tau^m.t9*exp(-del^m.p9);
        alphar10 = m.n10*del^(m.d10-2.0)*((m.d10-m.p10*del^m.p10)*(m.d10-1.0-m.p10*del^m.p10)-m.p10^2.0*del^m.p10)*tau^m.t10*exp(-del^m.p10);
        alphar11 = m.n11*del^(m.d11-2.0)*((m.d11-m.p11*del^m.p11)*(m.d11-1.0-m.p11*del^m.p11)-m.p11^2.0*del^m.p11)*tau^m.t11*exp(-del^m.p11);
        alphar12 = m.n12*del^(m.d12-2.0)*((m.d12-m.p12*del^m.p12)*(m.d12-1.0-m.p12*del^m.p12)-m.p12^2.0*del^m.p12)*tau^m.t12*exp(-del^m.p12);
        alphar = alphar1+alphar2+alphar6+alphar7+alphar8+alphar9+alphar10+alphar11+alphar12;
        return alphar;
    end
#--------------------------------------------------------------------------
    # Function to calculate differential of alpha residual w.r.t tau
    function dalfrtau(tau,del)
        alphar1 = m.n1*m.t1*del^m.d1*tau^(m.t1-1.0)+m.n2*m.t2*del^m.d2*tau^(m.t2-1.0)+m.n3*m.t3*del^m.d3*tau^(m.t3-1.0);
        alphar2 = m.n4*m.t4*del^m.d4*tau^(m.t4-1.0)+m.n5*m.t5*del^m.d5*tau^(m.t5-1.0)+m.n6*m.t6*del^m.d6*tau^(m.t6-1.0);
        alphar3 = m.n7*m.t7*del^m.d7*tau^(m.t7-1.0)*exp(-del^m.p7);
        alphar4 = m.n8*m.t8*del^m.d8*tau^(m.t8-1.0)*exp(-del^m.p8)+m.n9*m.t9*del^m.d9*tau^(m.t9-1.0)*exp(-del^m.p9);
        alphar5 = m.n10*m.t10*del^m.d10*tau^(m.t10-1.0)*exp(-del^m.p10)+m.n11*m.t11*del^m.d11*tau^(m.t11-1.0)*exp(-del^m.p11);
        alphar6 = m.n12*m.t12*del^m.d12*tau^(m.t12-1.0)*exp(-del^m.p12);
        alphar = alphar1+alphar2+alphar3+alphar4+alphar5+alphar6;
        return alphar;
    end
#--------------------------------------------------------------------------
    # Function to calculate double differential of alpha residual w.r.t tau
    function d2alfrtau(tau,del)
        alphar1 = m.n1*m.t1*(m.t1-1.0)*del^m.d1*tau^(m.t1-2.0)+m.n2*m.t2*(m.t2-1.0)*del^m.d2*tau^(m.t2-2.0)+m.n3*m.t3*(m.t3-1.0)*del^m.d3*tau^(m.t3-2.0);
        alphar2 = m.n4*m.t4*(m.t4-1.0)*del^m.d4*tau^(m.t4-2.0)+m.n5*m.t5*(m.t5-1.0)*del^m.d5*tau^(m.t5-2.0)+m.n6*m.t6*(m.t6-1.0)*del^m.d6*tau^(m.t6-2.0);
        alphar3 = m.n7*m.t7*(m.t7-1.0)*del^m.d7*tau^(m.t7-2.0)*exp(-del^m.p7);
        alphar4 = m.n8*m.t8*(m.t8-1.0)*del^m.d8*tau^(m.t8-2.0)*exp(-del^m.p8)+m.n9*m.t9*(m.t9-1.0)*del^m.d9*tau^(m.t9-2.0)*exp(-del^m.p9);
        alphar5 = m.n10*m.t10*(m.t10-1.0)*del^m.d10*tau^(m.t10-2.0)*exp(-del^m.p10)+m.n11*m.t11*(m.t11-1.0)*del^m.d11*tau^(m.t11-2.0)*exp(-del^m.p11);
        alphar6 = m.n12*m.t12*(m.t12-1.0)*del^m.d12*tau^(m.t12-2.0)*exp(-del^m.p12);
        alphar = alphar1+alphar2+alphar3+alphar4+alphar5+alphar6;
        return alphar;
    end
#--------------------------------------------------------------------------
    # Function to calculate double differential of alpha residual w.r.t tau and delta
    function d2alfrdeltau(tau,del)
        alphar1 = m.n1*m.d1*m.t1*del^(m.d1-1.0)*tau^(m.t1-1.0)+m.n2*m.d2*m.t2*del^(m.d2-1.0)*tau^(m.t2-1.0)+m.n3*m.d3*m.t3*del^(m.d3-1.0)*tau^(m.t3-1.0);
        alphar2 = m.n4*m.d4*m.t4*del^(m.d4-1.0)*tau^(m.t4-1.0)+m.n5*m.d5*m.t5*del^(m.d5-1.0)*tau^(m.t5-1.0)+m.n6*m.d6*m.t6*del^(m.d6-1.0)*tau^(m.t6-1.0);
        alphar3 = m.n7*m.t7*del^(m.d7-1.0)*(m.d7-m.p7*del^m.p7)*tau^(m.t7-1.0)*exp(-del^m.p7);
        alphar4 = m.n8*m.t8*del^(m.d8-1.0)*(m.d8-m.p8*del^m.p8)*tau^(m.t8-1.0)*exp(-del^m.p8)+m.n9*m.t9*del^(m.d9-1.0)*(m.d9-m.p9*del^m.p9)*tau^(m.t9-1.0)*exp(-del^m.p9);
        alphar5 = m.n10*m.t10*del^(m.d10-1.0)*(m.d10-m.p10*del^m.p10)*tau^(m.t10-1.0)*exp(-del^m.p10)+m.n11*m.t11*del^(m.d11-1.0)*(m.d11-m.p11*del^m.p11)*tau^(m.t11-1.0)*exp(-del^m.p11);
        alphar6 = m.n12*m.t12*del^(m.d12-1.0)*(m.d12-m.p12*del^m.p12)*tau^(m.t12-1.0)*exp(-del^m.p12)
        alphar = alphar1+alphar2+alphar3+alphar4+alphar5+alphar6;
        return alphar;
    end
#--------------------------------------------------------------------------
    # Function to calculate standard state alphao
    function alfo(tau,del)
        alphao1 = log(del)+m.a1+m.a2*tau+m.a3*log(tau);
        alphao2 = m.a4*log(1.0-exp(-tau*m.th4))+m.a5*log(1.0-exp(-tau*m.th5))+m.a6*log(1.0-exp(-tau*m.th6));
        alphao3 = m.a7*log(1.0-exp(-tau*m.th7))+m.a8*log(1.0-exp(-tau*m.th8));
        alphao = alphao1+alphao2+alphao2;
        return alphao
    end
#--------------------------------------------------------------------------
    # Function to calculate differential of alphao w.r.t tau

    function dalfotau(tau,del)
        alphao1 = m.a2+m.a3/tau;
        alphao2 = m.a4*m.th4*(1.0/(1.0-exp(-tau*m.th4))-1.0)+m.a5*m.th5*(1.0/(1.0-exp(-tau*m.th5))-1.0)+m.a6*m.th6*(1.0/(1.0-exp(-tau*m.th6))-1.0);
        alphao3 = m.a7*m.th7*(1.0/(1.0-exp(-tau*m.th7))-1.0)+m.a8*m.th8*(1.0/(1.0-exp(-tau*m.th8))-1.0);
        alphao = alphao1+alphao2+alphao3;
        return alphao;
    end

#--------------------------------------------------------------------------
    # Function to calculate double differential of alphao w.r.t tau
    function d2alfotau(tau,del)
        alphao1 = -m.a3/tau^2.0-m.a4*m.th4^2.0*exp(-tau*m.th4)*(1.0/(1.0-exp(-tau*m.th4))^2.0);
        alphao2 = m.a5*m.th5^2.0*exp(-tau*m.th5)*(1.0/(1.0-exp(-tau*m.th5))^2.0)+m.a6*m.th6^2.0*exp(-tau*m.th6)*(1.0/(1.0-exp(-tau*m.th6))^2.0);
        alphao3 = m.a7*m.th7^2.0*exp(-tau*m.th7)*(1.0/(1.0-exp(-tau*m.th7))^2.0)+m.a8*m.th8^2.0*exp(-tau*m.th8)*(1.0/(1.0-exp(-tau*m.th8))^2.0);
        alphao = alphao1-alphao2-alphao3;
        return alphao;
    end
#--------------------------------------------------------------------------
    # Function to calculate differential of alphao w.r.t delta
    function dalfodel(tau,del)
        alphao = 1.0/del;
        return alphao;
    end
#--------------------------------------------------------------------------
    # Function to calculate double differential of alphao w.r.t delta
    function d2alfodel(tau,del)
        alphao = -1.0/del^2.0;
        return alphao;
    end
#--------------------------------------------------------------------------
# Function to solve for density
# Newton-Raphson algorithm over del. Will generate two different
# results based on initial guesses.
function densol(p,t,dguess)
    count2 = 0;
    maxval = 100;
    dold = dguess;
    dnew = 0.1;
    tau = m.tc/t;
    tol = 1.0e-7;
    fold = (1000.0*p/m.rhoc/dold/m.r/t)-1.0-dold*dalfrdel(tau,dold);
    fdash = -(1000.0*p/m.rhoc/dold^2.0/m.r/t)-dalfrdel(tau,dold)-dold*d2alfrdel(tau,dold);
    for i in 1:maxval
        if dold < 0.0 || dold > 3.0
            if dguess < 1.0
                dnew = 0.4;
                count2 = count2 +1;
            else
                dnew = 1.3;
                count2 = count2 +1;
            end
        else
            dnew = dold-fold/fdash;
        end
        fnew = (1000.0*p/m.rhoc/dnew/m.r/t)-1.0-dnew*dalfrdel(tau,dnew);
        fdash = -(1000.0*p/m.rhoc/dnew^2.0/m.r/t)-dalfrdel(tau,dnew)-dnew*d2alfrdel(tau,dnew);
        if abs(fnew) < tol
            break
        else
            dold = dnew;
            fold = fnew;
        end
        if i == maxval || count2 == 2
            dnew = 10.0;
            break
        end
    end
    del = dnew;
    fugacity = exp(alfr(tau,del)+del*dalfrdel(tau,del)-log(1.0+del*dalfrdel(tau,del)));
    den = del*m.rhoc;
    return den, fugacity;
end
#--------------------------------------------------------------------------
# Phase equilibrium solver
function swdench4(p,t)
    (deng, fugg) = densol(p,t,0.001);
    (denl, fugl) = densol(p,t,2.0);
    # Check if the densities are the same
    errd::Float64 = abs(deng-denl)::Float64;
    if errd < 1.0e-3
        if deng <= m.rhoceos
            fugl = 10000000.0;
        else
            fugg = 10000000.0;
        end
    end
    errf::Float64 = abs(fugg-fugl);
    if errf < 1.0e-7
        deng = deng;
        denl = denl;
    elseif fugg < fugl
        deng = deng;
        denl = 0.0;
    else
        deng = 0.0;
        denl = denl;
    end
    #den::Tuple{deng::Float64,::Float64};
    den = (deng,denl);      # density is in kg/m3
    #println("density = $den")
    #den = deng;
    return den;
end
#--------------------------------------------------------------------------
#Fugacity calculator
function swfugch4(p,t)
    den = swdench4(p,t)
    tau = m.tc/t;
    del = den./m.rhoc;
    fugacity = zeros(2);
    for i in 1:2
        if del[i] == 0.0
            fugacity[i] = 0.0;
        else
            fugacity[i] = exp(alfr(tau,del[i])+del[i]*dalfrdel(tau,del[i])-log(1.0+del[i]*dalfrdel(tau,del[i])));
        end
    end
    return fugacity
end
#--------------------------------------------------------------------------
#Enthalpy
function swenthch4(p,t)
    den = swdench4(p,t)
    tau = m.tc/t;
    del = den./m.rhoc;
    enthalpy = zeros(2);
    for i in 1:2
        if del[i] == 0.0
            enthalpy[i] = 0.0;
        else
            enthalpy[i] = m.r*t*(1.0+tau*(dalfotau(tau,del[i])+dalfrtau(tau,del[i]))+del[i]*dalfrdel(tau,del[i]));
        end
    end
    entout = (enthalpy[1], enthalpy[2])
    return entout
end
#--------------------------------------------------------------------------
# Internal energy
function swintech4(p,t)
    den = swdench4(p,t)
    tau = m.tc/t;
    del = den./m.rhoc;
    intenergy = zeros(2);
    for i in 1:2
        if del[i] == 0.0
            intenergy[i] = 0.0;
        else
            intenergy[i] = m.r*t*tau*(dalfotau(tau,del[i])+dalfrtau(tau,del[i]));
        end
    end
    intout = (intenergy[1], intenergy[2])
    return intout
end
#--------------------------------------------------------------------------
# Entropy
function swentch4(p,t)
    den = swdench4(p,t)
    tau = m.tc/t;
    del = den./m.rhoc;
    entropy = zeros(2);
    for i in 1:2
        if del[i] == 0.0
            entropy[i] = 0.0;
        else
            entropy[i] = m.r*tau*(dalfotau(tau,del[i])+dalfrtau(tau,del[i]))-m.r*(alfo(tau,del[i])+alfr(tau,del[i]));
        end
    end
    return entropy
end
#--------------------------------------------------------------------------
# constant volume heat capacity
function swcvch4(p,t)
    den = swdench4(p,t)
    tau = m.tc/t;
    del = den./m.rhoc;
    cv = zeros(2);
    for i in 1:2
        if del[i] == 0.0
            cv[i] = 0.0
        else
            cv[i] = -m.r*tau^2.0*(d2alfotau(tau,del[i])+d2alfrtau(tau,del[i]));
        end
    end
    return cv
end
#--------------------------------------------------------------------------
# constant pressure heat capacity
function swcpch4(p,t)
    den = swdench4(p,t)
    tau = m.tc/t;
    del = den./m.rhoc;
    cp = zeros(2);
    for i in 1:2
        if del[i] == 0.0
            cp[i] = 0.0
        else
            cp[i] = -m.r*tau^2.0*(d2alfotau(tau,del[i])+d2alfrtau(tau,del[i]))+m.r*(1.0+del[i]*dalfrdel(tau,del[i])-tau*del[i]*d2alfrdeltau(tau,del[i]))^2.0/(1.0+2.0*del[i]*dalfrdel(tau,del[i])+del[i]^2.0*d2alfrdel(tau,del[i]));
        end
    end
    return cp
end
#--------------------------------------------------------------------------
# co2 viscosity
function swviscch4(p,t)
    den = swdench4(p,t)
    visc = zeros(2);
    epsi = 251.196;
    tstar = t/epsi;
    #zero-density limit
    lnt = log(tstar);
    lngstar = 0.235156-0.491266*lnt+0.05211155*lnt^2.0+0.05347906*lnt^3.0-0.01537102*lnt^4.0;
    gstar = exp(lngstar);
    eta0 = 1.00697*t^0.5/gstar;
    for i in 1:2
        if den[i] == 0.0
            visc[i] = 0.0
        else
            visc[i] = eta0+0.004071119*den[i]+0.7198037*10^-4*den[i]^2.0+0.2411697*10^-16*den[i]^6.0/tstar^3.0+0.2971072*10^-22*den[i]^8.0-0.1627888*10^-22*den[i]^8.0/tstar;
        end
    end
    return visc*1e-6
end
#--------------------------------------------------------------------------
# ch4 hydrate formation pressure in seawater conditions
function pch4h(x::Number)
    #28term
    pd = -6.96924868978287e11 + 1.459249338808763e10*x - 1.1139635111596729e8*x^2 + 334574.69514012313*x^3 - 69.59034876232835*x^4 - 1.6295116911436311*x^5 + 0.0069554053300216285*x^6 - 1.2075697129416482e-5*x^7 - 1.444961713096671e-7*x^8 + 4.1301112526149986e-10*x^9 + 9.091466197300102e-13*x^10 + 2.951785194978322e-15*x^11 - 2.4491065403351783e-17*x^12 - 2.8747240160636505e-20*x^13 + 2.366964455314111e-22*x^14 - 3.5625160853892175e-25*x^15 - 1.267175643997602e-27*x^16 + 6.502162773377947e-30*x^17 + 2.3400312305145585e-32*x^18 + 3.883455683917119e-36*x^19 - 7.257740248144519e-37*x^20 + 3.531500476922321e-40*x^21 + 5.777728592845599e-42*x^22 - 8.25714605641091e-45*x^23 + 4.848551318699393e-47*x^24 - 3.3677444371285043e-49*x^25 + 7.978968280967026e-52*x^26 - 7.151600867443923e-55*x^27 + 1.5581994398332717e-58*x^28
    return pd
end
#--------------------------------------------------------------------------
